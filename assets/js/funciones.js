

/*Loader*/
var showLoader = document.getElementById('showLoader');
function confirmarCliente() {
    showLoader.classList.remove('displayNone');
    setTimeout(() => {
        showLoader.classList.add('displayNone');
        window.location.href = "confirmar-cliente.html";
    }, 2000);

}

var showLoader2 = document.getElementById('showLoader2');
function montoPagar() {
    showLoader2.classList.remove('displayNone');
    setTimeout(() => {
        showLoader2.classList.add('displayNone');
        window.location.href = "ingresar-monto.html";
    }, 2000);

}

var showLoader3 = document.getElementById('showLoader3');
function siImprimir() {
    showLoader3.classList.remove('displayNone');
    setTimeout(() => {
        showLoader3.classList.add('displayNone');
        window.location.href = "abono-exitoso.html";
    }, 2000);

}


function soloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}


var numCuenta1 = [];
var numCuenta2 = [];
var numCuenta3 = [];
var numCuenta4 = [];

$(".inputTeclado").on('mousedown', function (e) {
    e.preventDefault();
});

$(".inputTecladoTarjeta").on('mousedown', function (e) {
    e.preventDefault();
});

$(".errorNumCliente").css("display", "none");
$(".errorNumCelular").css("display", "flex");
$(".errorNumCelular2").css("display", "none");
$(".ocultarConfirmarNumero").css("display", "none");
$(".btnContinuar").css("background-color", "#A6A6A6");


function agregarNumero(num) {
    var num1 = document.getElementById('numCuenta1').value;
    var num2 = document.getElementById('numCuenta2').value;
    var num3 = document.getElementById('numCuenta3').value;
    var num4 = document.getElementById('numCuenta4').value;
    if (numCuenta1.length < 4) {
        $('.inputTeclado').css('border', '1px solid #ED872A');
        $(".errorNumCliente").css("display", "flex");
        numCuenta1.push(num);
        $('#numCuenta1').val(numCuenta1.join(""));
        setTimeout(function () {
            $("#numCuenta1").val($("#numCuenta1").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numCuenta2.length < 4) {
        $('.inputTeclado').css('border', '1px solid #ED872A');
        $(".errorNumCliente").css("display", "flex");
        numCuenta2.push(num);
        $('#numCuenta2').val(numCuenta2.join(""));
        setTimeout(function () {
            $("#numCuenta2").val($("#numCuenta2").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numCuenta3.length < 4) {
        $('.inputTeclado').css('border', '1px solid #ED872A');
        $(".errorNumCliente").css("display", "flex");
        numCuenta3.push(num);
        $('#numCuenta3').val(numCuenta3.join(""));
        setTimeout(function () {
            $("#numCuenta3").val($("#numCuenta3").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numCuenta4.length < 4) {
        $('.inputTeclado').css('border', '1px solid #ED872A');
        $(".errorNumCliente").css("display", "flex");
        numCuenta4.push(num);
        $('#numCuenta4').val(numCuenta4.join(""));

        if (numCuenta1.length + numCuenta2.length + numCuenta3.length + numCuenta4.length === 16) {
            $('.inputTeclado').css('border', '1px solid #17A54D');
            $(".errorNumCliente").css("display", "none");
        } else if (numCuenta1.length + numCuenta2.length + numCuenta3.length + numCuenta4.length < 16) {
            $('.inputTeclado').css('border', '1px solid #ED872A');
            $(".errorNumCliente").css("display", "flex");
        }
    }
}

document.body.addEventListener('click', function (e) {
    if (e.target.className === 'inputTeclado') {
        e.preventDefault();
    }
});


var numTarjeta1 = [];
var numTarjeta2 = [];
var numTarjeta3 = [];
var numTarjeta4 = [];

$(".errorNumTarjeta").css("display", "none");
function agregarNumeroTarjeta(num) {
    var num1 = document.getElementById('numTarjeta1').value;
    var num2 = document.getElementById('numTarjeta2').value;
    var num3 = document.getElementById('numTarjeta3').value;
    var num4 = document.getElementById('numTarjeta4').value;
    if (numTarjeta1.length < 4) {
        numTarjeta1.push(num);
        $('.inputTecladoTarjeta').css('border', '1px solid #ED872A');
        $(".errorNumTarjeta").css("display", "flex");
        $('#numTarjeta1').val(numTarjeta1.join(""));
        setTimeout(function () {
            $("#numTarjeta1").val($("#numTarjeta1").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numTarjeta2.length < 4) {
        numTarjeta2.push(num);
        $('.inputTecladoTarjeta').css('border', '1px solid #ED872A');
        $(".errorNumTarjeta").css("display", "flex");
        $('#numTarjeta2').val(numTarjeta2.join(""));
        setTimeout(function () {
            $("#numTarjeta2").val($("#numTarjeta2").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numTarjeta3.length < 4) {
        numTarjeta3.push(num);
        $('.inputTecladoTarjeta').css('border', '1px solid #ED872A');
        $(".errorNumTarjeta").css("display", "flex");
        $('#numTarjeta3').val(numTarjeta3.join(""));
        setTimeout(function () {
            $("#numTarjeta3").val($("#numTarjeta3").val().replace(/./g, "•"));
        }, 3000);
    }
    else if (numTarjeta4.length < 4) {
        numTarjeta4.push(num);
        $('#numTarjeta4').val(numTarjeta4.join(""));
        $('.inputTecladoTarjeta').css('border', '1px solid #ED872A');
        $(".errorNumTarjeta").css("display", "flex");
        if (numTarjeta1.length + numTarjeta2.length + numTarjeta3.length + numTarjeta4.length === 16) {
            $('.inputTecladoTarjeta').css('border', '1px solid #17A54D');
            $(".errorNumTarjeta").css("display", "none");
        } else if (numTarjeta1.length + numTarjeta2.length + numTarjeta3.length + numTarjeta4.length < 16) {
            $('.inputTecladoTarjeta').css('border', '1px solid #ED872A');
            $(".errorNumTarjeta").css("display", "flex");
        }
    }
}

document.body.addEventListener('click', function (e) {
    if (e.target.className === 'inputTecladoTarjeta') {
        e.preventDefault();
    }
});


/*Otra cantidad*/
$(".inputTecladoCantidad").on('mousedown', function (e) {
    e.preventDefault();
});

var inputOtraCantidad = [];
function agregarOtraCantidad(num) {
    var num1 = document.getElementById('inputOtraCantidad').value;
    // var valor = $('#inputOtraCantidad').val().replace("$","");
    if (inputOtraCantidad.length < 10) {
        inputOtraCantidad.push(num);
        $('#inputOtraCantidad').val(inputOtraCantidad.join(""));
        if (num1 == "") {
            $('#inputOtraCantidad').css('border', '1px solid #ED872A');
        } else {
            $('#inputOtraCantidad').css('border', '1px solid #17A54D');

        }
    }
}


document.body.addEventListener('click', function (e) {
    if (e.target.className === 'inputTecladoCantidad') {
        e.preventDefault();
    }
});

function borrarNumero() {
    numCuenta1.splice(0, 4);
    numCuenta2.splice(0, 4);
    numCuenta3.splice(0, 4);
    numCuenta4.splice(0, 4);
    $('#numCuenta1').val(numCuenta1.join(""));
    $('#numCuenta2').val(numCuenta2.join(""));
    $('#numCuenta3').val(numCuenta3.join(""));
    $('#numCuenta4').val(numCuenta4.join(""));
    $('#numCuenta1').focus();
    // $('#btnAceptarCuenta').css('display', 'none');
}

function borrarNumeroTarjeta() {
    numTarjeta1.splice(0, 4);
    numTarjeta2.splice(0, 4);
    numTarjeta3.splice(0, 4);
    numTarjeta4.splice(0, 4);
    $('#numTarjeta1').val(numTarjeta1.join(""));
    $('#numTarjeta2').val(numTarjeta2.join(""));
    $('#numTarjeta3').val(numTarjeta3.join(""));
    $('#numTarjeta4').val(numTarjeta4.join(""));
    $('#numTarjeta1').focus();
    // $('#btnAceptarCuenta').css('display', 'none');
}

function borrarNumeroCantidad() {
    inputOtraCantidad.splice(0, 1);
    $("#inputOtraCantidad").val(inputOtraCantidad.join(""));
}

function resetValue() {
    $('.inputTeclado').val('');
    $('.inputTecladoTarjeta').val('');
}




function montoSnCambio() {
    window.location.href = "sin-cambio.html";
}
function pagoAbonado() {
    window.location.href = "abono-exitoso.html";
}
function seleccionarMonto() {
    window.location.href = "seleccionar-pedido.html";
}

function irInicio() {
    window.location.href = "inicio.html";
}

function irPasoUno() {
    window.location.href = "confirmar-cliente.html";
}
function irPasoDos() {
    window.location.href = "seleccionar-monto.html";
}
function irPasoTres() {
    window.location.href = "ingresar-monto.html";
}
function irIndex() {
    window.location.href = "index.html";
    resetValue();
}

$(".emojibtn img").click(function () {
    $('.emojibtn img').removeClass("u-opacity-1");
    $(this).addClass("u-opacity-1");
})

function finalizarProceso() {
    window.location.href = "hasta-pronto.html";
}

function pagoRealizar() {
    window.location.href = "pago-realizar.html";
}
function irSeleccionarPedido() {
    window.location.href = "seleccionar-pedido.html";
}
function irSeleccionarMonto() {
    window.location.href = "seleccionar-monto.html";
}
function irClienteBuscar() {
    window.location.href = "confirmar-cliente.html";
}
function irSeleccionarOperacion() {
    window.location.href = "seleccionar-operacion.html";
}

function ingresarNumero() {
    window.location.href = "ingresar-numero-recarga.html";
}
$(".errorMonto").css("display", "none");
$(".avisoCantidad").css("display", "none");
var inputOtraCantidad = [];
function agregarOtraCantidad(num) {
    var num1 = document.getElementById('inputOtraCantidad').value;
    if (inputOtraCantidad.length < 12) {
        inputOtraCantidad.push(num);
        $('#inputOtraCantidad').val(inputOtraCantidad.join(""));
        if (num1 >= "100") {
            $('#inputOtraCantidad').css('border', '1px solid #ED872A');
            $(".errorMonto").css("display", "flex");
            $(".avisoCantidad").css("display", "block");
        } else {
            $('#inputOtraCantidad').css('border', '1px solid #17A54D');
            $(".errorMonto").css("display", "none");
            $(".avisoCantidad").css("display", "none");
        }
    }
}

function borrarOtraCantidad() {
    inputOtraCantidad.splice(0, 1);
    $("#inputOtraCantidad").val(inputOtraCantidad.join(""));
}
function cerrarAviso() {
    $(".contAviso").css("display", "none");
}
function cerrarAviso2() {
    $(".contAvisoCredito").css("display", "none");
}

$(".inputNumeroTeclado").on('mousedown', function (e) {
    e.preventDefault();
});


var inputNumeroTeclado = [];
var inputNumeroTeclado2 = [];
function agregarNumeroTeclado(num) {
    var num1 = document.getElementById('inputNumeroTeclado').value.length;
    var num2 = document.getElementById('inputNumeroTeclado2').value.length;
    var btnContinuar = document.getElementById('btnContinuar');
    if (inputNumeroTeclado.length < 10) {
        inputNumeroTeclado.push(num);
        if (inputNumeroTeclado.length === 10) {
            $(".ocultarConfirmarNumero").css("display", "grid");
            $(".inputNumeroTeclado2").css("display", "grid");
            console.log("entre")
        }
        $('#inputNumeroTeclado').val(inputNumeroTeclado.join(""));
        if (num1 == "") {
            $('.inputNumeroTeclado').css('border', '1px solid #ED872A');
            $(".errorNumCelular").css("display", "flex");
            $(".ocultarConfirmarNumero").css("display", "none");
            $(".btnContinuar").css("background-color", "#A6A6A6");
        } else {
            $('.inputNumeroTeclado').css('border', '1px solid #17A54D');
            $(".errorNumCelular").css("display", "none");
            $(".inputNumeroTeclado2").focus();
        }
    } else {
        if (inputNumeroTeclado2.length < 10) {
            inputNumeroTeclado2.push(num);
            $('#inputNumeroTeclado2').val(inputNumeroTeclado2.join(""));
            if (num1 != num2) {
                $(".errorNumCelular2").css("display", "flex");
            }

            if (num1 === num2) {
                $(".errorNumCelular2").css("display", "none");
                $(".btnContinuar").css("background-color", "#17A54D");

            }
        }


    }

    console.log("num " + num1)
    console.log(inputNumeroTeclado)
    console.log(inputNumeroTeclado2)
}
$(".inputNumeroTeclado2").on('mousedown', function (e) {
    e.preventDefault();
});
document.body.addEventListener('click', function (e) {
    if (e.target.className === 'inputNumeroTeclado2') {
        e.preventDefault();
    }
});
function corregirNumeroTeclado() {
    inputNumeroTeclado.splice(-1, 1);
    $("#inputNumeroTeclado").val(inputNumeroTeclado.join(""));
}
function borrarNumeroTeclado() {
    inputNumeroTeclado.splice(0, 10);
    $('#inputNumeroTeclado').val(inputNumeroTeclado.join(""));
    $('.inputNumeroTeclado').css('border', '1px solid #ED872A');
    $(".errorNumCelular").css("display", "flex");
    $(".ocultarConfirmarNumero").css("display", "none");
    $(".btnContinuar").css("background-color", "#A6A6A6");
}